import DisplayObject = PIXI.DisplayObject;
import * as utils from '../Utils'
import * as i from "../Interfaces";
import {timeConfig} from "../Utils";

const TWEEN = require('@tweenjs/tween.js');


export default class MainScene extends PIXI.Container {
    private app: any;
    // private animations: Array<Function> = [];
    private model: any;
    private main_container: PIXI.Container;

    public port: Port;
    public docks: Dock[];
    public ships: Ship[];

    constructor(app) {
        super();
        this.app = app;
        this.port = new Port(app.model);
        this.model = app.model;
        this.main_container = new PIXI.Container()
        this.docks = [];
        this.ships = [];

        // this.addChild(this.port.rect);
        // this.addChild(this.port.entry_rect);
        this.app.stage.addChild(this.main_container);
        this.main_container.addChild(this.port.lower_rect);
        this.main_container.addChild(this.port.upper_rect);
        this.createDocks()
        this.addDocksEvents()

        function animate(time) {
            requestAnimationFrame(animate);
            try {
                TWEEN.update(time);
            } catch (e) {
                console.log('TW error', e)
            }
        }

        requestAnimationFrame(animate);
    }

    public createDocks() {
        this.model.port.getAllDocks().forEach((dock_model, index) => {
            this.docks[index] = new Dock({
                coord: dock_model.coordinates,
                size: dock_model.size,
                loaded: dock_model.loaded
            });
            this.main_container.addChild(this.docks[index].rect);
        })

    }

    public addShipView(ship_model): Ship {
        let ship = new Ship({
            coord: ship_model.coordinates,
            size: ship_model.size,
            loaded: ship_model.ship_loaded,
            ship_id: ship_model.ship_id,
        }, ship_model);
        this.addNewShip(ship);

        this.main_container.addChild(ship.rect);
        return ship
    }

    private addNewShip(ship: Ship) {
        this.addShipEvents(ship)
        this.ships.push(ship);

    }

    public executeMoving(self, ship_id) {
        let tween = new TWEEN.Tween(self.ships[ship_id].position)
            .to(self.ships[ship_id].destination, self.ships[ship_id].trip_time * 1000)
            .easing(TWEEN.Easing.Quadratic.Out)
            .onUpdate(function (ddd) {
                self.ships[ship_id].rect.x = ddd.x - self.ships[ship_id].start_position.start_x;
                self.ships[ship_id].rect.y = ddd.y - self.ships[ship_id].start_position.start_y;
            })
            .onComplete(function () {
                // console.log('onComplete!!!')
                // self.ships[ship_id].start_position.start_x = self.ships[ship_id].position.x;
                // self.ships[ship_id].start_position.start_y = self.ships[ship_id].position.y;
                self.ships[ship_id].rect.x = self.ships[ship_id].position.x - self.ships[ship_id].start_position.start_x;
                self.ships[ship_id].rect.y = self.ships[ship_id].position.y - self.ships[ship_id].start_position.start_y;
                self.ships[ship_id].endMove();
            })
            .start();
    }

    public addDocksEvents() {
        this.docks.forEach((dock, index) => {
            this.model.events.on(`${utils.dockEvents.loaded}${index}`, () => {
                this.docks[index].rect.tint = utils.colors.loaded_dock;
            });

            this.model.events.on(`${utils.dockEvents.unloaded}${index}`, () => {
                this.docks[index].rect.tint = utils.colors.empty_dock;
            });
        });
    }

    public addShipEvents(ship: Ship) {
        ship.model.events.on(`ready_to_go_${ship.ship_id}`, () => {
            this.executeMoving(this, ship.ship_id)
        });
        // ship.model.events.on(`end_move_${ship.ship_id}`, () => {
        //     this.endShipMove(ship.ship_id)
        // });
    }
}


class Dock {
    rect: PIXI.Graphics;

    constructor(options: any) {
        this.rect = new PIXI.Graphics()
            .lineStyle(3, utils.colors.common_line, 1) // границы - толщина, цвет, прозрачность
            .beginFill(0xffffff, 1)
            .drawRect(options.coord.x, options.coord.y, options.size.width, options.size.height);

        this.rect.interactive = true;
        this.rect.pivot.set(this.rect.width / 2, this.rect.height / 2);

        if (options.loaded) {
            this.rect.tint = utils.colors.loaded_dock;
        } else
            this.rect.tint = utils.colors.empty_dock;
    }
}

class Ship {
    start_position: any;
    position: i.Coordinates;
    destination: i.Coordinates;
    trip_time: any;
    label: PIXI.Text;
    model: any;

    ship_id: number;
    next_event_trigger: string;
    rect: PIXI.Graphics;


    constructor(options, ship_model) {
        this.start_position = {start_x: options.coord.x, start_y: options.coord.y};
        this.rect = new PIXI.Graphics()
            .lineStyle(3, utils.colors.common_line, 1) // границы - толщина, цвет, прозрачность
            .beginFill(0xffffff, 1)
            .drawRect(options.coord.x, options.coord.y, options.size.width, options.size.height);

        this.rect.pivot.set(this.rect.width / 2, this.rect.height / 2);
        if (options.loaded) {
            this.rect.tint = utils.colors.loaded_ship;
            this.rect.alpha = 1;
        } else {
            this.rect.tint = utils.colors.empty_ship;
            this.rect.alpha = 0.5;
        }


        this.model = ship_model;
        this.ship_id = options.ship_id;
        this.label = new PIXI.Text(`${this.ship_id}`, {
            fontFamily: 'Arial',
            fontSize: 28,
            fill: 0xffffff,
            align: 'center'
        })
        this.label.position.x = options.coord.x + this.rect.width / 2;
        this.label.position.y = options.coord.y + this.rect.height / 2;
        this.label.anchor.set(.5);
        this.setPosition(options.coord);
        this.rect.addChild(this.label);

        this.model.events.on(utils.shipEvents.empty, () => {
            this.rect.alpha = 0.5;//utils.colors.empty_ship
        });
        this.model.events.on(utils.shipEvents.loaded, () => {
            this.rect.alpha = 1;//utils.colors.loaded_dock
        });
    }

    public endMove() {
        this.model.events.trigger(this.next_event_trigger);
    }

    public setTriggerName(trigger) {
        this.next_event_trigger = trigger;
    }

    public movingBegin(time: timeConfig) {
        this.trip_time = time;
        this.model.events.trigger(`ready_to_go_${this.ship_id}`);
    }

    public setDestination(destination: i.Coordinates) {
        this.destination = destination;
    }

    public setPosition(position: i.Coordinates) {
        this.position = position;
    }


    public destroyShip() {
        // this.rect.destroy()
    }

}

class Port {
    lower_rect: PIXI.Graphics;
    upper_rect: PIXI.Graphics;

    constructor(model) {
        this.upper_rect = new PIXI.Graphics()
            .lineStyle(3, utils.colors.common_line, 1) // границы - толщина, цвет, прозрачность
            .beginFill(utils.colors.sea)
            .drawRect(190, 0, 25, 300);
        this.lower_rect = new PIXI.Graphics()
            .lineStyle(3, utils.colors.common_line, 1) // границы - толщина, цвет, прозрачность
            .beginFill(utils.colors.sea)
            .drawRect(190, 450, 25, 600);
    }
}