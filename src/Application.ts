import Controller from './controllers/Controller';
import * as View from './views';
import Model from './model/Model';

export default class Application extends PIXI.Application {

    public controller;
    public model;
    public view;

    public scene: any; // field for access to current scene

    constructor(options: Object) {
        super(options);
        this.model = new Model();

        this.scene = new View.MainScene(this);
        this.controller = new Controller(this.model, this.scene);

    }

    public theEnd() {
        this.stage.removeChildren();
        this.stage.destroy();
        PIXI.utils.destroyTextureCache();
        this.destroy(true);
    }
}