import * as i from "../Interfaces";
import * as utils from '../Utils'

export default class Model {
    events: EventSubscribe;
    ships: ShipModel[] = [];
    public ships_count: number;
    port: PortModel;
    queue: QueueModel;

    constructor() {
        this.port = new PortModel(this);
        this.queue = new QueueModel();
        this.events = new EventSubscribe()
    }

    addShipModel(options) {

        let ship = new ShipModel(options);
        this.ships.push(ship)
        return ship
    }
}


class EventSubscribe implements i.ILiteEvent {
    public handlers: { event: string, handler: Function, params: any }[] = [];

    public on(event: string, handler: Function, params?: any) {
        // let _params = params ? params : []
        this.handlers.push({event, handler, params});
    }

    public off(rm_event: string, handler: { (data?: any): void }): void {
        this.handlers = this.handlers.filter((event) => {
            return event.event !== rm_event
        });
    }

    public trigger(emit_event: string, params?: any) {
        this.handlers.slice(0).forEach((event) => {
            if (emit_event === event.event) {
                event.params = params ? params : event.params;
                event.handler(event.params);
            }
        });
    }

    public expose(): i.ILiteEvent {
        return this;
    }
}


class PortModel {
    docks: { loaded: boolean, frozen: boolean, coordinates: i.Coordinates, size: any, handle_point: i.Coordinates }[] = [];
    g_model: Model;
    events: EventSubscribe;
    port_can_enter: number;
    port_enter_points: { enter: i.Coordinates, passed_enter: i.Coordinates };

    constructor(model) {
        this.g_model = model;
        this.port_can_enter = undefined;
        this.events = new EventSubscribe();
        this.port_enter_points = {enter: utils.coordinates.portEnter, passed_enter: utils.coordinates.portPassEnter}

        this.docks.push({
            loaded: false,
            frozen: false,
            coordinates: {x: 0, y: 120},
            size: {width: 60, height: 110},
            handle_point: {x: 70, y: 120}
        });
        this.docks.push({
            loaded: false,
            frozen: false,
            coordinates: {x: 0, y: 270},
            size: {width: 60, height: 110},
            handle_point: {x: 70, y: 270}
        });
        this.docks.push({
            loaded: false,
            frozen: false,
            coordinates: {x: 0, y: 420},
            size: {width: 60, height: 110},
            handle_point: {x: 70, y: 420}
        });
        this.docks.push({
            loaded: false,
            frozen: false,
            coordinates: {x: 0, y: 570},
            size: {width: 60, height: 110},
            handle_point: {x: 70, y: 570}
        });
    }

    public freezeDock(index) {
        this.docks[index].frozen = true;
        this.g_model.events.trigger(`${utils.dockEvents.freeze}${index}`);
    }

    public unfreezeDock(index) {
        this.docks[index].frozen = false;
    }

    public getAllDocks() {
        return this.docks;
    };

    public getEnterPoint(): i.Coordinates {
        return this.port_enter_points.enter
    }

    public getPassEnterPoint(): i.Coordinates {
        return this.port_enter_points.passed_enter
    }

    public getDockInfo(index: number, name: string) {
        if (!this.docks[index] || !this.docks[index].hasOwnProperty(name))
            return 'No info';
        return this.docks[index][name];
    }

    public toggleCargoDock(dock_id) {
        this.unfreezeDock(dock_id);
        if (this.docks[dock_id].loaded) {
            this.unloadDock(dock_id)
        } else {
            this.loadDock(dock_id)
        }
    }

    loadDock(id): void {
        this.docks[id].loaded = true;
        this.notifyQueue(id, utils.portEvents.dockLoaded, utils.dockEvents.loaded, true)
    }

    unloadDock(id): void {
        this.docks[id].loaded = false;
        this.notifyQueue(id, utils.portEvents.dockUnloaded, utils.dockEvents.unloaded, false)
    }

    private notifyQueue(dock_id: number, port_event: utils.portEvents, dock_event: utils.dockEvents, loaded: boolean) {
        this.events.trigger(`${port_event}`, {dock_id: dock_id, dock_loaded: loaded})
        this.g_model.events.trigger(`${dock_event}${dock_id}`)
    }


}

class ShipModel {
    events: EventSubscribe;
    ship_loaded: boolean;
    size: any;
    ship_id: number;
    coordinates: i.Coordinates;

    constructor(options) {
        this.ship_loaded = options.loaded;
        this.size = options.size;
        this.coordinates = options.coordinates;
        this.ship_id = options.ship_id;
        this.events = new EventSubscribe()
    }

    toggleShipLoaded() {
        if (this.ship_loaded) {
            this.unloadShip()
        } else {
            this.loadShip()
        }
    }

    loadShip(): void {
        this.ship_loaded = true;
        this.events.trigger(`${utils.shipEvents.loaded}`)
    }

    unloadShip(): void {
        this.ship_loaded = false;
        this.events.trigger(`${utils.shipEvents.empty}`)
    }
}

class QueueModel {
    queue: any[];
    time_queue: any[];
    queue_type: any;

    time_queue_enter_pos: i.Coordinates;
    time_queue_exit_pos: i.Coordinates;
    cargo_queue_next_pos: i.Coordinates;

    constructor() {
        this.queue = [];
        this.time_queue = [];
        this.queue_type = undefined;
        this.time_queue_enter_pos = utils.coordinates.timeQueueEnter;
        this.time_queue_exit_pos = utils.coordinates.timeQueueExit;
        this.cargo_queue_next_pos = utils.coordinates.cargoQueuePos;
    }

    public addToCargoQueue(ship) {
        this.queue.push(ship);
        this.queue_type = ship.model.ship_loaded;
        this.cargo_queue_next_pos.x += utils.coordinates.queueShift.x;
    }

    public getFromCargoQueue() {
        this.cargo_queue_next_pos.x -= utils.coordinates.queueShift.x;
        return this.queue.shift();

    }

    public isCargoQueueExist(timeout?: number) {
        if (timeout) {
            return setTimeout(() => {
                return this.queue.length > 0;
            }, timeout)
        }
        else return this.queue.length > 0;
    }

    public addToTimeQueue(ship, index?: number) {
        // this.time_queue_enter_pos.x += 80;
        if (index) {
            this.time_queue.splice(index, 0, ship)
        }
        else {
            this.time_queue.push(ship);
        }

    }

    public getFromTimeQueue(handle_type: boolean) {
        // this.time_queue_enter_pos.x -= 80;
        return this.time_queue.shift();
    }

    public getTimeQNextPlace(direction: utils.passEnterDirection) {
        let position = direction === utils.passEnterDirection.enter ? this.time_queue_enter_pos : this.time_queue_exit_pos;
        return position;
    }

    public getCargoQNextPlace() {
        return this.cargo_queue_next_pos;
    }

    public timeQueueIsEmpty(): Boolean {
        return !this.time_queue.length;
    }

}