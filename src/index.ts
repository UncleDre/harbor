import * as PIXI from 'pixi.js';
import Application from './Application';
PIXI.utils.skipHello();

new class Main {
    app: Application;

    settings: PIXI.ApplicationOptions;

    constructor () {
        this.settings = {
            width: document.body.clientWidth,
            height: document.body.clientHeight,
            backgroundColor: 0x6DD3DB,
        }

        const app = new Application(this.settings);
        document.body.appendChild(app.view);
    }
}