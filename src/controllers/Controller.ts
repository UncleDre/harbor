import * as utils from '../Utils'
import * as i from "../Interfaces";
import {timeConfig} from "../Utils";


export default class Controller {
    model: any;
    view: any;
    produce_ship: boolean;
    ships: shipController[] = [];
    queue: queueController;
    time_queue: timeQueueController;
    port: portController;

    constructor(model, view) {
        this.model = model;
        this.view = view;
        this.queue = new queueController(model, this);
        this.time_queue = new timeQueueController(model, this);
        this.port = new portController({g_model: model});
        this.produce_ship = true;

        this.produceShips();
    }

    produceShips() {
        setInterval(() => {
            // if (this.ships.length > 29)
            //     return;

            let ship_model = this.model.addShipModel(this.shipModelParams());
            let ship_view = this.view.addShipView(ship_model);

            let ship = new shipController(ship_model, ship_view, this);

            this.ships.push(ship);
            ship.moveToPort(utils.timeConfig.toPort);
        }, utils.timeConfig.shipCreate)
    }


    private shipModelParams() {
        let coordinates = {x: 1200, y: Math.random() * 200};
        return {
            size: {width: 80, height: 50},
            coordinates: coordinates,
            loaded: (Math.random() >= 0.5),
            ship_id: this.ships.length
        }
    }

    askPortForNextStep(ship_id: number) {
        if (this.queue.shipTypeQueueExist(this.ships[ship_id].model.ship_loaded)) {
            this.queue.addToCargoQueue(this.ships[ship_id]);
            this.ships[ship_id].moveToCargoQueue()
            return;
        } else {
            this.askPortForDock(ship_id)
        }
    }

    askPortForDock(ship_id: number) {
        let port_answer = this.tryGetDock(!this.ships[ship_id].model.ship_loaded);

        if (port_answer.result) {
            let chosen_dock = port_answer.docks[0];
            this.port.bookDock(chosen_dock);
            this.ships[ship_id].action_dock_id = chosen_dock;
            this.ships[ship_id].view.model.events.trigger('arrive_enter_point');
        }
        else if (port_answer.reason === 'no_available') {
            this.queue.addToCargoQueue(this.ships[ship_id]);
            this.ships[ship_id].moveToCargoQueue();
        }
    }

    tryGetDock(handle_type) {
        let port_answer = this.port.getFreeDockIds(handle_type);
        return port_answer;
    }

    askPortForEnter(ship_id: number) {
        let answer = this.port.mayShipEnter(ship_id);

        if (answer) {
            this.ships[ship_id].moveToPassEnter()
        } else {
            this.ships[ship_id].moveToTimeQueue(utils.passEnterDirection.enter)
        }
    }

    askPortForLeave(ship_id: number) {
        let answer = this.port.mayShipEnter(ship_id);

        if (answer) {
            this.ships[ship_id].view.setTriggerName('passed_enter_to_exit')
            this.ships[ship_id].view.setDestination(utils.coordinates.portEnter);
            this.ships[ship_id].view.movingBegin(utils.timeConfig.passEnter);
        } else {
            this.time_queue.addToTimeQueue(this.ships[ship_id], 0);
            this.ships[ship_id].moveToExitQueue(utils.passEnterDirection.exit)
        }
    }

    cargoHandle(ship_id: number) {
        let _ship_id = ship_id
        this.ships[_ship_id].model.toggleShipLoaded();
        this.model.port.toggleCargoDock(this.ships[_ship_id].action_dock_id);

        this.ships[_ship_id].view.setTriggerName('arrive_out_point');
        this.ships[_ship_id].view.setDestination(utils.coordinates.portPassEnter);
        this.ships[_ship_id].view.movingBegin(utils.timeConfig.fromDock)
    }

    removeShip(ship_id: number) {
        // console.log('removeShip', ship_id)
        this.ships[ship_id].view.destroyShip()
    }

    public activateWaitingShip(ship: shipController) {
        if (ship.exit_port) {
            ship.view.setTriggerName('arrive_out_point');
            ship.view.setDestination(utils.coordinates.portPassEnter);
            ship.view.movingBegin(utils.timeConfig.toQueue)
        } else
            this.ships[ship.g_controller_id].moveToPortEnter()
    }

    public activateCargoShip(ship: shipController) {
        this.ships[ship.g_controller_id].toPortFromCargoQueue(utils.timeConfig.fromQueue)
    }

}

class shipController {
    model: any;
    view: any;
    g_controller: Controller;
    g_controller_id: number;
    action_dock_id: number;
    exit_port: Boolean;

    constructor(ship_model: any, ship_view: any, g_controller) {
        this.model = ship_model;
        this.view = ship_view;
        this.g_controller = g_controller;
        this.g_controller_id = g_controller.ships.length;
        this.exit_port = false;

        this.addActionsSequence();
    }

    public addAction(action: string, callback: Function, parameters: any) {
        this.view.model.events.on(action, callback, parameters);
    }

    public addActionsSequence() {
        this.addAction('arrive_to_port', this.g_controller.askPortForNextStep.bind(this.g_controller), this.g_controller_id);
        this.addAction('arrive_from_cargo_queue', this.g_controller.askPortForEnter.bind(this.g_controller), this.g_controller_id);
        this.addAction('arrive_enter_point', this.g_controller.askPortForEnter.bind(this.g_controller), this.g_controller_id);
        this.addAction('passed_enter_point', this.moveToDock.bind(this), this.g_controller_id);
        this.addAction('arrive_to_dock', this.g_controller.cargoHandle.bind(this.g_controller), this.g_controller_id);
        this.addAction('finalize_voyage', this.g_controller.removeShip.bind(this.g_controller), this.g_controller_id);
        this.addAction('arrive_out_point', this.moveToPortExit.bind(this), this.g_controller_id);
        this.addAction('passed_enter_to_exit', this.goOutPort.bind(this), this.g_controller_id);
        this.addAction('arrive_to_time_queue', this.arriveToTimeQueue.bind(this), this.g_controller_id);
    }

    public goOutPort() {
        this.g_controller.port.unfreezeEnter();
        this.view.setDestination(utils.coordinates.finalVoyage);
        this.view.setTriggerName('finalize_voyage')
        this.view.movingBegin(utils.timeConfig.toPort);
    }

    public moveToPort(time: timeConfig) {
        this.view.setDestination(utils.coordinates.portArrival);
        this.view.setTriggerName('arrive_to_port')
        this.view.movingBegin(time);
    }

    public toPortFromCargoQueue(time: timeConfig) {
        this.view.setDestination(this.g_controller.model.port.getEnterPoint());
        this.view.setTriggerName('arrive_from_cargo_queue')
        this.view.movingBegin(time);
    }

    public moveToDock() {
        this.g_controller.port.unfreezeEnter();
        this.view.setTriggerName('arrive_to_dock')
        this.view.setDestination(this.g_controller.model.port.getDockInfo(this.action_dock_id, 'handle_point'));
        // this.action_dock_id = chosen_dock;
        this.view.movingBegin(utils.timeConfig.toDock)
    }

    public arriveToTimeQueue() {
        this.g_controller.time_queue.addToTimeQueue(this);
    }

    public moveToExitQueue(direction?: utils.passEnterDirection) {
        this.view.setDestination(this.g_controller.time_queue.getNextPlace(direction));
        this.view.movingBegin(utils.timeConfig.toQueue);
    }

    public moveToTimeQueue(direction?: utils.passEnterDirection) {
        this.view.setDestination(this.g_controller.time_queue.getNextPlace(direction));
        this.view.setTriggerName('arrive_to_time_queue')
        this.view.movingBegin(utils.timeConfig.toQueue);
    }

    public moveToCargoQueue() {
        this.view.setDestination(this.g_controller.queue.getNextPlace());
        this.view.setTriggerName('')
        this.view.movingBegin(utils.timeConfig.toQueue);
    }

    public moveToPortEnter() {
        this.view.setTriggerName('arrive_enter_point')
        this.view.setDestination(this.g_controller.port.model.getEnterPoint());
        this.view.movingBegin(utils.timeConfig.fromQueue);
    }

    public moveToPassEnter() {
        this.view.setTriggerName('passed_enter_point')
        this.view.setDestination(this.g_controller.port.model.getPassEnterPoint());
        this.view.movingBegin(utils.timeConfig.passEnter);

    }

    public moveToPortExit(ship_id: number) {
        this.view.setTriggerName('')
        this.exit_port = true;//flag for activate after time queue func
        this.g_controller.askPortForLeave(ship_id)
    }
}

class portController {
    model: any;

    constructor(options: any) {
        this.model = options.g_model.port

    }

    public freezeEnter(ship_id) {
        this.model.port_can_enter = ship_id;

    }

    public unfreezeEnter() {
        this.model.port_can_enter = undefined;
        this.model.events.trigger('unfreezePortEnter')
    }

    public mayShipEnter(ship_id: number) {
        if (this.model.port_can_enter === undefined) {
            this.freezeEnter(ship_id)
            return true
        }
        return this.model.port_can_enter === ship_id;

    }

    getFreeDockIds(action): i.DockAnswer {
        return this.findAvailableDocks(!!action);
    }

    findAvailableDocks(search_type: Boolean): i.DockAnswer {
        let result: number[] = [];
        let reason = 'no_available';

        for (let i = 0; i < this.model.docks.length; i++) {
            if (this.model.docks[i].loaded === search_type && !this.model.docks[i].frozen) {
                result.push(i);
                reason = '';
            }
        }

        return {docks: result, result: !!result.length, reason: reason}
    };

    public bookDock(dock_id) {
        this.model.freezeDock(dock_id);
    }
}

class queueController {
    queue_type: any;
    g_model: any;
    queue_model: any;
    g_controller: any;

    constructor(model, controller) {
        this.g_model = model
        this.queue_model = model.queue;
        this.g_controller = controller;
        this.subscribeOnPortEvent(utils.portEvents.dockUnloaded);
        this.subscribeOnPortEvent(utils.portEvents.dockLoaded);

    }

    public getNextPlace() {
        return this.queue_model.getCargoQNextPlace()
    }

    public addToCargoQueue(ship) {
        this.queue_model.addToCargoQueue(ship)
        this.queue_type = this.queue_model.queue_type;
    }

    public getFromCargoQueue() {
        let ship = this.queue_model.getFromCargoQueue();
        return ship
    }

    private shiftCargoQueueView() {
        this.queue_model.queue.forEach((ship) => {
            ship.view.setDestination({x: ship.view.position.x - 80, y: ship.view.position.y});
            ship.view.movingBegin(utils.timeConfig.toQueue);
        })
    }

    public shipTypeQueueExist(ship_type, timeout?) {
        return this.queue_model.queue_type === ship_type && this.queue_model.isCargoQueueExist(timeout)
    }

    public subscribeOnPortEvent(type) {
        this.g_model.port.events.on(type, (params) => {
            if (this.queue_model.isCargoQueueExist() && this.queue_model.queue_type !== params.dock_loaded) {
                let answer = this.g_controller.tryGetDock(!this.queue_model.queue_type);
                if (answer.result) {
                    this.g_controller.port.bookDock(answer.docks[0]);
                    let ship = this.getFromCargoQueue();
                    ship.action_dock_id = answer.docks[0];
                    this.shiftCargoQueueView();
                    this.g_controller.activateCargoShip(ship)
                }
            }
        });
    }


    public unsubscribeOnDockEvent(type) {
        this.g_model.port.events.off(type, (params) => {
            this.g_controller.activateWaitingShip(this.getFromCargoQueue())
        });
    }
}

class timeQueueController {
    g_model: any;
    queue_model: any;
    g_controller: any;

    constructor(model, controller) {
        this.g_model = model
        this.queue_model = model.queue;
        this.g_controller = controller;
        this.subscribeOnEnterUnfreeze()
    }

    public addToTimeQueue(ship, index?: number) {
        this.queue_model.addToTimeQueue(ship, index)
    }

    public getFromTimeQueue() {
        let ship = this.queue_model.getFromTimeQueue();
        return ship;
    }

    public getNextPlace(direction: utils.passEnterDirection) {
        return this.queue_model.getTimeQNextPlace(direction)
    }

    public subscribeOnEnterUnfreeze() {
        this.g_model.port.events.on('unfreezePortEnter', (params) => {
            if (!this.queue_model.timeQueueIsEmpty()) {
                let next_ship = this.getFromTimeQueue();
                if (this.g_controller.port.mayShipEnter(next_ship.g_controller_id)) {
                    this.g_controller.activateWaitingShip(next_ship);
                }
                else {
                    this.addToTimeQueue(next_ship, 0)
                }
            }
        });

    }
}