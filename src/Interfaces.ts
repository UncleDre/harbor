interface Coordinates {
    x:number,
    y:number,
}

interface ILiteEvent {
    on(event: string, handler: { (data?: any): void }, params?:any);

    off(event: string, handler: { (data?: any): void });
}

interface DockAnswer {
    docks: number[];
    result: Boolean;
    reason: string;
}

export {Coordinates, ILiteEvent, DockAnswer}