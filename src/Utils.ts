import * as i from "./Interfaces";
import {Coordinates} from "./Interfaces";

enum colors {
    sea = 0x6DD3DB,
    white = 0xffffff,
    loaded_dock = 0xe20000,
    empty_dock = 0x6DD3DB,
    loaded_ship = 0xe20000,
    empty_ship = 0x008800,
    common_line = 0xe20000,
    entry_line = 0x000000,
    port_pass = 0xe22300
}

enum timeConfig {
    toPort = 4,
    toDock = 3,
    passEnter = 1,
    toQueue = 0.5,
    fromQueue = 1,
    fromDock = 2,
    handleDock = 2,
    shipCreate = 8000,
}

enum dockEvents {
    loaded = 'loaded_dock_',
    unloaded = 'empty_dock_',
    freeze = 'freeze_dock_',
    unfreeze = 'unfreeze_dock_',
}

enum portEvents {
    dockLoaded = 'loaded_dock',
    dockUnloaded = 'empty_dock',
}

enum passEnterDirection {
    exit = 'exit',
    enter = 'enter',
}

enum shipEvents {
    loaded = 'loaded_ship_',
    empty = 'empty_ship_',
}

const coordinates = {
    portEnter: {x: 240, y: 380},
    portArrival: {x: 280, y: 380},
    portPassEnter: {x: 140, y: 380},
    finalVoyage: {x: 1600, y: 400},
    queueShift: {x: 80, y: 0},
    timeQueueEnter: {x: 280, y: 290},
    timeQueueExit: {x: 140, y: 290},
    cargoQueuePos: {x: 280, y: 480},
}


export {colors, timeConfig, dockEvents, shipEvents, portEvents, passEnterDirection, coordinates}