"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var Controller_1 = require("./controllers/Controller");
var View = require("./views");
var Model_1 = require("./model/Model");
var Application = (function (_super) {
    __extends(Application, _super);
    function Application(options) {
        var _this = _super.call(this, options) || this;
        _this.model = new Model_1.default();
        _this.scene = new View.MainScene(_this);
        _this.controller = new Controller_1.default(_this.model, _this.scene);
        return _this;
    }
    Application.prototype.theEnd = function () {
        this.stage.removeChildren();
        this.stage.destroy();
        PIXI.utils.destroyTextureCache();
        this.destroy(true);
    };
    return Application;
}(PIXI.Application));
exports.default = Application;
//# sourceMappingURL=Application.js.map