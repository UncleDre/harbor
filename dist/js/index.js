"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var PIXI = require("pixi.js");
var Application_1 = require("./Application");
PIXI.utils.skipHello();
new (function () {
    function Main() {
        this.settings = {
            width: document.body.clientWidth,
            height: document.body.clientHeight,
            backgroundColor: 0x6DD3DB,
        };
        var app = new Application_1.default(this.settings);
        document.body.appendChild(app.view);
    }
    return Main;
}());
//# sourceMappingURL=index.js.map