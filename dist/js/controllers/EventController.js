"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var EventController = (function () {
    function EventController() {
    }
    EventController.prototype.declare = function (event, callback) {
        this[event] = callback;
    };
    EventController.prototype.get = function (event) {
        return this[event];
    };
    EventController.prototype.emit = function (event) {
        console.log();
        this[event]();
    };
    return EventController;
}());
exports.default = EventController;
//# sourceMappingURL=EventController.js.map