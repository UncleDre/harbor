"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var utils = require("../Utils");
var Controller = (function () {
    function Controller(model, view) {
        this.ships = [];
        this.model = model;
        this.view = view;
        this.queue = new queueController(model, this);
        this.time_queue = new timeQueueController(model, this);
        this.port = new portController({ g_model: model });
        this.produce_ship = true;
        this.produceShips();
    }
    Controller.prototype.produceShips = function () {
        var _this = this;
        setInterval(function () {
            var ship_model = _this.model.addShipModel(_this.shipModelParams());
            var ship_view = _this.view.addShipView(ship_model);
            var ship = new shipController(ship_model, ship_view, _this);
            _this.ships.push(ship);
            ship.moveToPort(utils.timeConfig.toPort);
        }, utils.timeConfig.shipCreate);
    };
    Controller.prototype.shipModelParams = function () {
        var coordinates = { x: 1200, y: Math.random() * 200 };
        return {
            size: { width: 80, height: 50 },
            coordinates: coordinates,
            loaded: (Math.random() >= 0.5),
            ship_id: this.ships.length
        };
    };
    Controller.prototype.askPortForNextStep = function (ship_id) {
        if (this.queue.shipTypeQueueExist(this.ships[ship_id].model.ship_loaded)) {
            this.queue.addToCargoQueue(this.ships[ship_id]);
            this.ships[ship_id].moveToCargoQueue();
            return;
        }
        else {
            this.askPortForDock(ship_id);
        }
    };
    Controller.prototype.askPortForDock = function (ship_id) {
        var port_answer = this.tryGetDock(!this.ships[ship_id].model.ship_loaded);
        if (port_answer.result) {
            var chosen_dock = port_answer.docks[0];
            this.port.bookDock(chosen_dock);
            this.ships[ship_id].action_dock_id = chosen_dock;
            this.ships[ship_id].view.model.events.trigger('arrive_enter_point');
        }
        else if (port_answer.reason === 'no_available') {
            this.queue.addToCargoQueue(this.ships[ship_id]);
            this.ships[ship_id].moveToCargoQueue();
        }
    };
    Controller.prototype.tryGetDock = function (handle_type) {
        var port_answer = this.port.getFreeDockIds(handle_type);
        return port_answer;
    };
    Controller.prototype.askPortForEnter = function (ship_id) {
        var answer = this.port.mayShipEnter(ship_id);
        if (answer) {
            this.ships[ship_id].moveToPassEnter();
        }
        else {
            this.ships[ship_id].moveToTimeQueue(utils.passEnterDirection.enter);
        }
    };
    Controller.prototype.askPortForLeave = function (ship_id) {
        var answer = this.port.mayShipEnter(ship_id);
        if (answer) {
            this.ships[ship_id].view.setTriggerName('passed_enter_to_exit');
            this.ships[ship_id].view.setDestination(utils.coordinates.portEnter);
            this.ships[ship_id].view.movingBegin(utils.timeConfig.passEnter);
        }
        else {
            this.time_queue.addToTimeQueue(this.ships[ship_id], 0);
            this.ships[ship_id].moveToExitQueue(utils.passEnterDirection.exit);
        }
    };
    Controller.prototype.cargoHandle = function (ship_id) {
        var _ship_id = ship_id;
        this.ships[_ship_id].model.toggleShipLoaded();
        this.model.port.toggleCargoDock(this.ships[_ship_id].action_dock_id);
        this.ships[_ship_id].view.setTriggerName('arrive_out_point');
        this.ships[_ship_id].view.setDestination(utils.coordinates.portPassEnter);
        this.ships[_ship_id].view.movingBegin(utils.timeConfig.fromDock);
    };
    Controller.prototype.removeShip = function (ship_id) {
        this.ships[ship_id].view.destroyShip();
    };
    Controller.prototype.activateWaitingShip = function (ship) {
        if (ship.exit_port) {
            ship.view.setTriggerName('arrive_out_point');
            ship.view.setDestination(utils.coordinates.portPassEnter);
            ship.view.movingBegin(utils.timeConfig.toQueue);
        }
        else
            this.ships[ship.g_controller_id].moveToPortEnter();
    };
    Controller.prototype.activateCargoShip = function (ship) {
        this.ships[ship.g_controller_id].toPortFromCargoQueue(utils.timeConfig.fromQueue);
    };
    return Controller;
}());
exports.default = Controller;
var shipController = (function () {
    function shipController(ship_model, ship_view, g_controller) {
        this.model = ship_model;
        this.view = ship_view;
        this.g_controller = g_controller;
        this.g_controller_id = g_controller.ships.length;
        this.exit_port = false;
        this.addActionsSequence();
    }
    shipController.prototype.addAction = function (action, callback, parameters) {
        this.view.model.events.on(action, callback, parameters);
    };
    shipController.prototype.addActionsSequence = function () {
        this.addAction('arrive_to_port', this.g_controller.askPortForNextStep.bind(this.g_controller), this.g_controller_id);
        this.addAction('arrive_from_cargo_queue', this.g_controller.askPortForEnter.bind(this.g_controller), this.g_controller_id);
        this.addAction('arrive_enter_point', this.g_controller.askPortForEnter.bind(this.g_controller), this.g_controller_id);
        this.addAction('passed_enter_point', this.moveToDock.bind(this), this.g_controller_id);
        this.addAction('arrive_to_dock', this.g_controller.cargoHandle.bind(this.g_controller), this.g_controller_id);
        this.addAction('finalize_voyage', this.g_controller.removeShip.bind(this.g_controller), this.g_controller_id);
        this.addAction('arrive_out_point', this.moveToPortExit.bind(this), this.g_controller_id);
        this.addAction('passed_enter_to_exit', this.goOutPort.bind(this), this.g_controller_id);
        this.addAction('arrive_to_time_queue', this.arriveToTimeQueue.bind(this), this.g_controller_id);
    };
    shipController.prototype.goOutPort = function () {
        this.g_controller.port.unfreezeEnter();
        this.view.setDestination(utils.coordinates.finalVoyage);
        this.view.setTriggerName('finalize_voyage');
        this.view.movingBegin(utils.timeConfig.toPort);
    };
    shipController.prototype.moveToPort = function (time) {
        this.view.setDestination(utils.coordinates.portArrival);
        this.view.setTriggerName('arrive_to_port');
        this.view.movingBegin(time);
    };
    shipController.prototype.toPortFromCargoQueue = function (time) {
        this.view.setDestination(this.g_controller.model.port.getEnterPoint());
        this.view.setTriggerName('arrive_from_cargo_queue');
        this.view.movingBegin(time);
    };
    shipController.prototype.moveToDock = function () {
        this.g_controller.port.unfreezeEnter();
        this.view.setTriggerName('arrive_to_dock');
        this.view.setDestination(this.g_controller.model.port.getDockInfo(this.action_dock_id, 'handle_point'));
        this.view.movingBegin(utils.timeConfig.toDock);
    };
    shipController.prototype.arriveToTimeQueue = function () {
        this.g_controller.time_queue.addToTimeQueue(this);
    };
    shipController.prototype.moveToExitQueue = function (direction) {
        this.view.setDestination(this.g_controller.time_queue.getNextPlace(direction));
        this.view.movingBegin(utils.timeConfig.toQueue);
    };
    shipController.prototype.moveToTimeQueue = function (direction) {
        this.view.setDestination(this.g_controller.time_queue.getNextPlace(direction));
        this.view.setTriggerName('arrive_to_time_queue');
        this.view.movingBegin(utils.timeConfig.toQueue);
    };
    shipController.prototype.moveToCargoQueue = function () {
        this.view.setDestination(this.g_controller.queue.getNextPlace());
        this.view.setTriggerName('');
        this.view.movingBegin(utils.timeConfig.toQueue);
    };
    shipController.prototype.moveToPortEnter = function () {
        this.view.setTriggerName('arrive_enter_point');
        this.view.setDestination(this.g_controller.port.model.getEnterPoint());
        this.view.movingBegin(utils.timeConfig.fromQueue);
    };
    shipController.prototype.moveToPassEnter = function () {
        this.view.setTriggerName('passed_enter_point');
        this.view.setDestination(this.g_controller.port.model.getPassEnterPoint());
        this.view.movingBegin(utils.timeConfig.passEnter);
    };
    shipController.prototype.moveToPortExit = function (ship_id) {
        this.view.setTriggerName('');
        this.exit_port = true;
        this.g_controller.askPortForLeave(ship_id);
    };
    return shipController;
}());
var portController = (function () {
    function portController(options) {
        this.model = options.g_model.port;
    }
    portController.prototype.freezeEnter = function (ship_id) {
        this.model.port_can_enter = ship_id;
    };
    portController.prototype.unfreezeEnter = function () {
        this.model.port_can_enter = undefined;
        this.model.events.trigger('unfreezePortEnter');
    };
    portController.prototype.mayShipEnter = function (ship_id) {
        if (this.model.port_can_enter === undefined) {
            this.freezeEnter(ship_id);
            return true;
        }
        return this.model.port_can_enter === ship_id;
    };
    portController.prototype.getFreeDockIds = function (action) {
        return this.findAvailableDocks(!!action);
    };
    portController.prototype.findAvailableDocks = function (search_type) {
        var result = [];
        var reason = 'no_available';
        for (var i_1 = 0; i_1 < this.model.docks.length; i_1++) {
            if (this.model.docks[i_1].loaded === search_type && !this.model.docks[i_1].frozen) {
                result.push(i_1);
                reason = '';
            }
        }
        return { docks: result, result: !!result.length, reason: reason };
    };
    ;
    portController.prototype.bookDock = function (dock_id) {
        this.model.freezeDock(dock_id);
    };
    return portController;
}());
var queueController = (function () {
    function queueController(model, controller) {
        this.g_model = model;
        this.queue_model = model.queue;
        this.g_controller = controller;
        this.subscribeOnPortEvent(utils.portEvents.dockUnloaded);
        this.subscribeOnPortEvent(utils.portEvents.dockLoaded);
    }
    queueController.prototype.getNextPlace = function () {
        return this.queue_model.getCargoQNextPlace();
    };
    queueController.prototype.addToCargoQueue = function (ship) {
        this.queue_model.addToCargoQueue(ship);
        this.queue_type = this.queue_model.queue_type;
    };
    queueController.prototype.getFromCargoQueue = function () {
        var ship = this.queue_model.getFromCargoQueue();
        return ship;
    };
    queueController.prototype.shiftCargoQueueView = function () {
        this.queue_model.queue.forEach(function (ship) {
            ship.view.setDestination({ x: ship.view.position.x - 80, y: ship.view.position.y });
            ship.view.movingBegin(utils.timeConfig.toQueue);
        });
    };
    queueController.prototype.shipTypeQueueExist = function (ship_type, timeout) {
        return this.queue_model.queue_type === ship_type && this.queue_model.isCargoQueueExist(timeout);
    };
    queueController.prototype.subscribeOnPortEvent = function (type) {
        var _this = this;
        this.g_model.port.events.on(type, function (params) {
            if (_this.queue_model.isCargoQueueExist() && _this.queue_model.queue_type !== params.dock_loaded) {
                var answer = _this.g_controller.tryGetDock(!_this.queue_model.queue_type);
                if (answer.result) {
                    _this.g_controller.port.bookDock(answer.docks[0]);
                    var ship = _this.getFromCargoQueue();
                    ship.action_dock_id = answer.docks[0];
                    _this.shiftCargoQueueView();
                    _this.g_controller.activateCargoShip(ship);
                }
            }
        });
    };
    queueController.prototype.unsubscribeOnDockEvent = function (type) {
        var _this = this;
        this.g_model.port.events.off(type, function (params) {
            _this.g_controller.activateWaitingShip(_this.getFromCargoQueue());
        });
    };
    return queueController;
}());
var timeQueueController = (function () {
    function timeQueueController(model, controller) {
        this.g_model = model;
        this.queue_model = model.queue;
        this.g_controller = controller;
        this.subscribeOnEnterUnfreeze();
    }
    timeQueueController.prototype.addToTimeQueue = function (ship, index) {
        this.queue_model.addToTimeQueue(ship, index);
    };
    timeQueueController.prototype.getFromTimeQueue = function () {
        var ship = this.queue_model.getFromTimeQueue();
        return ship;
    };
    timeQueueController.prototype.getNextPlace = function (direction) {
        return this.queue_model.getTimeQNextPlace(direction);
    };
    timeQueueController.prototype.subscribeOnEnterUnfreeze = function () {
        var _this = this;
        this.g_model.port.events.on('unfreezePortEnter', function (params) {
            if (!_this.queue_model.timeQueueIsEmpty()) {
                var next_ship = _this.getFromTimeQueue();
                if (_this.g_controller.port.mayShipEnter(next_ship.g_controller_id)) {
                    _this.g_controller.activateWaitingShip(next_ship);
                }
                else {
                    _this.addToTimeQueue(next_ship, 0);
                }
            }
        });
    };
    return timeQueueController;
}());
//# sourceMappingURL=Controller.js.map