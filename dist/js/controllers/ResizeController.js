"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ResizeController = (function () {
    function ResizeController(app) {
        this.app = app;
    }
    ResizeController.prototype.resize = function () {
        var scaleFactor = Math.min(window.innerWidth / 1280, window.innerHeight / 800);
        var newWidth = Math.ceil(1280 * scaleFactor);
        var newHeight = Math.ceil(800 * scaleFactor);
        this.app.renderer.resize(newWidth, newHeight);
        this.app.scene.scale.set(scaleFactor);
    };
    return ResizeController;
}());
exports.default = ResizeController;
//# sourceMappingURL=ResizeController.js.map