"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var colors;
(function (colors) {
    colors[colors["sea"] = 7197659] = "sea";
    colors[colors["white"] = 16777215] = "white";
    colors[colors["loaded_dock"] = 14811136] = "loaded_dock";
    colors[colors["empty_dock"] = 7197659] = "empty_dock";
    colors[colors["loaded_ship"] = 14811136] = "loaded_ship";
    colors[colors["empty_ship"] = 34816] = "empty_ship";
    colors[colors["common_line"] = 14811136] = "common_line";
    colors[colors["entry_line"] = 0] = "entry_line";
    colors[colors["port_pass"] = 14820096] = "port_pass";
})(colors || (colors = {}));
exports.colors = colors;
var timeConfig;
(function (timeConfig) {
    timeConfig[timeConfig["toPort"] = 4] = "toPort";
    timeConfig[timeConfig["toDock"] = 3] = "toDock";
    timeConfig[timeConfig["passEnter"] = 1] = "passEnter";
    timeConfig[timeConfig["toQueue"] = 0.5] = "toQueue";
    timeConfig[timeConfig["fromQueue"] = 1] = "fromQueue";
    timeConfig[timeConfig["fromDock"] = 2] = "fromDock";
    timeConfig[timeConfig["handleDock"] = 2] = "handleDock";
    timeConfig[timeConfig["shipCreate"] = 8000] = "shipCreate";
})(timeConfig || (timeConfig = {}));
exports.timeConfig = timeConfig;
var dockEvents;
(function (dockEvents) {
    dockEvents["loaded"] = "loaded_dock_";
    dockEvents["unloaded"] = "empty_dock_";
    dockEvents["freeze"] = "freeze_dock_";
    dockEvents["unfreeze"] = "unfreeze_dock_";
})(dockEvents || (dockEvents = {}));
exports.dockEvents = dockEvents;
var portEvents;
(function (portEvents) {
    portEvents["dockLoaded"] = "loaded_dock";
    portEvents["dockUnloaded"] = "empty_dock";
})(portEvents || (portEvents = {}));
exports.portEvents = portEvents;
var passEnterDirection;
(function (passEnterDirection) {
    passEnterDirection["exit"] = "exit";
    passEnterDirection["enter"] = "enter";
})(passEnterDirection || (passEnterDirection = {}));
exports.passEnterDirection = passEnterDirection;
var shipEvents;
(function (shipEvents) {
    shipEvents["loaded"] = "loaded_ship_";
    shipEvents["empty"] = "empty_ship_";
})(shipEvents || (shipEvents = {}));
exports.shipEvents = shipEvents;
var coordinates = {
    portEnter: { x: 240, y: 380 },
    portArrival: { x: 280, y: 380 },
    portPassEnter: { x: 140, y: 380 },
    finalVoyage: { x: 1600, y: 400 },
    queueShift: { x: 80, y: 0 },
    timeQueueEnter: { x: 280, y: 290 },
    timeQueueExit: { x: 140, y: 290 },
    cargoQueuePos: { x: 280, y: 480 },
};
exports.coordinates = coordinates;
//# sourceMappingURL=Utils.js.map