"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var utils = require("../Utils");
var TWEEN = require('@tweenjs/tween.js');
var MainScene = (function (_super) {
    __extends(MainScene, _super);
    function MainScene(app) {
        var _this = _super.call(this) || this;
        _this.app = app;
        _this.port = new Port(app.model);
        _this.model = app.model;
        _this.main_container = new PIXI.Container();
        _this.docks = [];
        _this.ships = [];
        _this.app.stage.addChild(_this.main_container);
        _this.main_container.addChild(_this.port.lower_rect);
        _this.main_container.addChild(_this.port.upper_rect);
        _this.createDocks();
        _this.addDocksEvents();
        function animate(time) {
            requestAnimationFrame(animate);
            try {
                TWEEN.update(time);
            }
            catch (e) {
                console.log('TW error', e);
            }
        }
        requestAnimationFrame(animate);
        return _this;
    }
    MainScene.prototype.createDocks = function () {
        var _this = this;
        this.model.port.getAllDocks().forEach(function (dock_model, index) {
            _this.docks[index] = new Dock({
                coord: dock_model.coordinates,
                size: dock_model.size,
                loaded: dock_model.loaded
            });
            _this.main_container.addChild(_this.docks[index].rect);
        });
    };
    MainScene.prototype.addShipView = function (ship_model) {
        var ship = new Ship({
            coord: ship_model.coordinates,
            size: ship_model.size,
            loaded: ship_model.ship_loaded,
            ship_id: ship_model.ship_id,
        }, ship_model);
        this.addNewShip(ship);
        this.main_container.addChild(ship.rect);
        return ship;
    };
    MainScene.prototype.addNewShip = function (ship) {
        this.addShipEvents(ship);
        this.ships.push(ship);
    };
    MainScene.prototype.executeMoving = function (self, ship_id) {
        var tween = new TWEEN.Tween(self.ships[ship_id].position)
            .to(self.ships[ship_id].destination, self.ships[ship_id].trip_time * 1000)
            .easing(TWEEN.Easing.Quadratic.Out)
            .onUpdate(function (ddd) {
            self.ships[ship_id].rect.x = ddd.x - self.ships[ship_id].start_position.start_x;
            self.ships[ship_id].rect.y = ddd.y - self.ships[ship_id].start_position.start_y;
        })
            .onComplete(function () {
            self.ships[ship_id].rect.x = self.ships[ship_id].position.x - self.ships[ship_id].start_position.start_x;
            self.ships[ship_id].rect.y = self.ships[ship_id].position.y - self.ships[ship_id].start_position.start_y;
            self.ships[ship_id].endMove();
        })
            .start();
    };
    MainScene.prototype.addDocksEvents = function () {
        var _this = this;
        this.docks.forEach(function (dock, index) {
            _this.model.events.on("" + utils.dockEvents.loaded + index, function () {
                _this.docks[index].rect.tint = utils.colors.loaded_dock;
            });
            _this.model.events.on("" + utils.dockEvents.unloaded + index, function () {
                _this.docks[index].rect.tint = utils.colors.empty_dock;
            });
        });
    };
    MainScene.prototype.addShipEvents = function (ship) {
        var _this = this;
        ship.model.events.on("ready_to_go_" + ship.ship_id, function () {
            _this.executeMoving(_this, ship.ship_id);
        });
    };
    return MainScene;
}(PIXI.Container));
exports.default = MainScene;
var Dock = (function () {
    function Dock(options) {
        this.rect = new PIXI.Graphics()
            .lineStyle(3, utils.colors.common_line, 1)
            .beginFill(0xffffff, 1)
            .drawRect(options.coord.x, options.coord.y, options.size.width, options.size.height);
        this.rect.interactive = true;
        this.rect.pivot.set(this.rect.width / 2, this.rect.height / 2);
        if (options.loaded) {
            this.rect.tint = utils.colors.loaded_dock;
        }
        else
            this.rect.tint = utils.colors.empty_dock;
    }
    return Dock;
}());
var Ship = (function () {
    function Ship(options, ship_model) {
        var _this = this;
        this.start_position = { start_x: options.coord.x, start_y: options.coord.y };
        this.rect = new PIXI.Graphics()
            .lineStyle(3, utils.colors.common_line, 1)
            .beginFill(0xffffff, 1)
            .drawRect(options.coord.x, options.coord.y, options.size.width, options.size.height);
        this.rect.pivot.set(this.rect.width / 2, this.rect.height / 2);
        if (options.loaded) {
            this.rect.tint = utils.colors.loaded_ship;
            this.rect.alpha = 1;
        }
        else {
            this.rect.tint = utils.colors.empty_ship;
            this.rect.alpha = 0.5;
        }
        this.model = ship_model;
        this.ship_id = options.ship_id;
        this.label = new PIXI.Text("" + this.ship_id, {
            fontFamily: 'Arial',
            fontSize: 28,
            fill: 0xffffff,
            align: 'center'
        });
        this.label.position.x = options.coord.x + this.rect.width / 2;
        this.label.position.y = options.coord.y + this.rect.height / 2;
        this.label.anchor.set(.5);
        this.setPosition(options.coord);
        this.rect.addChild(this.label);
        this.model.events.on(utils.shipEvents.empty, function () {
            _this.rect.alpha = 0.5;
        });
        this.model.events.on(utils.shipEvents.loaded, function () {
            _this.rect.alpha = 1;
        });
    }
    Ship.prototype.endMove = function () {
        this.model.events.trigger(this.next_event_trigger);
    };
    Ship.prototype.setTriggerName = function (trigger) {
        this.next_event_trigger = trigger;
    };
    Ship.prototype.movingBegin = function (time) {
        this.trip_time = time;
        this.model.events.trigger("ready_to_go_" + this.ship_id);
    };
    Ship.prototype.setDestination = function (destination) {
        this.destination = destination;
    };
    Ship.prototype.setPosition = function (position) {
        this.position = position;
    };
    Ship.prototype.destroyShip = function () {
    };
    return Ship;
}());
var Port = (function () {
    function Port(model) {
        this.upper_rect = new PIXI.Graphics()
            .lineStyle(3, utils.colors.common_line, 1)
            .beginFill(utils.colors.sea)
            .drawRect(190, 0, 25, 300);
        this.lower_rect = new PIXI.Graphics()
            .lineStyle(3, utils.colors.common_line, 1)
            .beginFill(utils.colors.sea)
            .drawRect(190, 450, 25, 600);
    }
    return Port;
}());
//# sourceMappingURL=MainScene.js.map