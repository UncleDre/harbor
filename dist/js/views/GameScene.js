"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var GameScene = (function (_super) {
    __extends(GameScene, _super);
    function GameScene(app) {
        var _this = _super.call(this) || this;
        _this.animations = [];
        _this.app = app;
        var circle = new PIXI.Graphics()
            .beginFill(0xab12ba)
            .drawCircle(0, 0, 100);
        circle.pivot.set(circle.width / 2, circle.height / 2);
        circle.position.set(640, 400);
        circle.interactive = circle.buttonMode = true;
        circle.on('pointertap', function () {
            _this.app.eventController.emit('click');
        });
        _this.label = new PIXI.Text(_this.app.model.get('number'), {
            fontSize: '48px',
            fill: 0xffffff
        });
        _this.label.anchor.set(.5);
        circle.addChild(_this.label);
        _this.addChild(circle);
        app.ticker.add(function () { return _this.play(); });
        _this.on('added', function () {
            _this.app.model.set('scene_state', 'ready');
        });
        _this.app.stage.addChild(_this);
        return _this;
    }
    GameScene.prototype.play = function () {
        if (this.animations.length == 0)
            return;
        this.animations.forEach(function (animation) {
            animation();
        });
    };
    return GameScene;
}(PIXI.Container));
exports.default = GameScene;
//# sourceMappingURL=GameScene.js.map