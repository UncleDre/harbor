"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var utils = require("../Utils");
var Model = (function () {
    function Model() {
        this.ships = [];
        this.port = new PortModel(this);
        this.queue = new QueueModel();
        this.events = new EventSubscribe();
    }
    Model.prototype.addShipModel = function (options) {
        var ship = new ShipModel(options);
        this.ships.push(ship);
        return ship;
    };
    return Model;
}());
exports.default = Model;
var EventSubscribe = (function () {
    function EventSubscribe() {
        this.handlers = [];
    }
    EventSubscribe.prototype.on = function (event, handler, params) {
        this.handlers.push({ event: event, handler: handler, params: params });
    };
    EventSubscribe.prototype.off = function (rm_event, handler) {
        this.handlers = this.handlers.filter(function (event) {
            return event.event !== rm_event;
        });
    };
    EventSubscribe.prototype.trigger = function (emit_event, params) {
        this.handlers.slice(0).forEach(function (event) {
            if (emit_event === event.event) {
                event.params = params ? params : event.params;
                event.handler(event.params);
            }
        });
    };
    EventSubscribe.prototype.expose = function () {
        return this;
    };
    return EventSubscribe;
}());
var PortModel = (function () {
    function PortModel(model) {
        this.docks = [];
        this.g_model = model;
        this.port_can_enter = undefined;
        this.events = new EventSubscribe();
        this.port_enter_points = { enter: utils.coordinates.portEnter, passed_enter: utils.coordinates.portPassEnter };
        this.docks.push({
            loaded: false,
            frozen: false,
            coordinates: { x: 0, y: 120 },
            size: { width: 60, height: 110 },
            handle_point: { x: 70, y: 120 }
        });
        this.docks.push({
            loaded: false,
            frozen: false,
            coordinates: { x: 0, y: 270 },
            size: { width: 60, height: 110 },
            handle_point: { x: 70, y: 270 }
        });
        this.docks.push({
            loaded: false,
            frozen: false,
            coordinates: { x: 0, y: 420 },
            size: { width: 60, height: 110 },
            handle_point: { x: 70, y: 420 }
        });
        this.docks.push({
            loaded: false,
            frozen: false,
            coordinates: { x: 0, y: 570 },
            size: { width: 60, height: 110 },
            handle_point: { x: 70, y: 570 }
        });
    }
    PortModel.prototype.freezeDock = function (index) {
        this.docks[index].frozen = true;
        this.g_model.events.trigger("" + utils.dockEvents.freeze + index);
    };
    PortModel.prototype.unfreezeDock = function (index) {
        this.docks[index].frozen = false;
    };
    PortModel.prototype.getAllDocks = function () {
        return this.docks;
    };
    ;
    PortModel.prototype.getEnterPoint = function () {
        return this.port_enter_points.enter;
    };
    PortModel.prototype.getPassEnterPoint = function () {
        return this.port_enter_points.passed_enter;
    };
    PortModel.prototype.getDockInfo = function (index, name) {
        if (!this.docks[index] || !this.docks[index].hasOwnProperty(name))
            return 'No info';
        return this.docks[index][name];
    };
    PortModel.prototype.toggleCargoDock = function (dock_id) {
        this.unfreezeDock(dock_id);
        if (this.docks[dock_id].loaded) {
            this.unloadDock(dock_id);
        }
        else {
            this.loadDock(dock_id);
        }
    };
    PortModel.prototype.loadDock = function (id) {
        this.docks[id].loaded = true;
        this.notifyQueue(id, utils.portEvents.dockLoaded, utils.dockEvents.loaded, true);
    };
    PortModel.prototype.unloadDock = function (id) {
        this.docks[id].loaded = false;
        this.notifyQueue(id, utils.portEvents.dockUnloaded, utils.dockEvents.unloaded, false);
    };
    PortModel.prototype.notifyQueue = function (dock_id, port_event, dock_event, loaded) {
        this.events.trigger("" + port_event, { dock_id: dock_id, dock_loaded: loaded });
        this.g_model.events.trigger("" + dock_event + dock_id);
    };
    return PortModel;
}());
var ShipModel = (function () {
    function ShipModel(options) {
        this.ship_loaded = options.loaded;
        this.size = options.size;
        this.coordinates = options.coordinates;
        this.ship_id = options.ship_id;
        this.events = new EventSubscribe();
    }
    ShipModel.prototype.toggleShipLoaded = function () {
        if (this.ship_loaded) {
            this.unloadShip();
        }
        else {
            this.loadShip();
        }
    };
    ShipModel.prototype.loadShip = function () {
        this.ship_loaded = true;
        this.events.trigger("" + utils.shipEvents.loaded);
    };
    ShipModel.prototype.unloadShip = function () {
        this.ship_loaded = false;
        this.events.trigger("" + utils.shipEvents.empty);
    };
    return ShipModel;
}());
var QueueModel = (function () {
    function QueueModel() {
        this.queue = [];
        this.time_queue = [];
        this.queue_type = undefined;
        this.time_queue_enter_pos = utils.coordinates.timeQueueEnter;
        this.time_queue_exit_pos = utils.coordinates.timeQueueExit;
        this.cargo_queue_next_pos = utils.coordinates.cargoQueuePos;
    }
    QueueModel.prototype.addToCargoQueue = function (ship) {
        this.queue.push(ship);
        this.queue_type = ship.model.ship_loaded;
        this.cargo_queue_next_pos.x += utils.coordinates.queueShift.x;
    };
    QueueModel.prototype.getFromCargoQueue = function () {
        this.cargo_queue_next_pos.x -= utils.coordinates.queueShift.x;
        return this.queue.shift();
    };
    QueueModel.prototype.isCargoQueueExist = function (timeout) {
        var _this = this;
        if (timeout) {
            return setTimeout(function () {
                return _this.queue.length > 0;
            }, timeout);
        }
        else
            return this.queue.length > 0;
    };
    QueueModel.prototype.addToTimeQueue = function (ship, index) {
        if (index) {
            this.time_queue.splice(index, 0, ship);
        }
        else {
            this.time_queue.push(ship);
        }
    };
    QueueModel.prototype.getFromTimeQueue = function (handle_type) {
        return this.time_queue.shift();
    };
    QueueModel.prototype.getTimeQNextPlace = function (direction) {
        var position = direction === utils.passEnterDirection.enter ? this.time_queue_enter_pos : this.time_queue_exit_pos;
        return position;
    };
    QueueModel.prototype.getCargoQNextPlace = function () {
        return this.cargo_queue_next_pos;
    };
    QueueModel.prototype.timeQueueIsEmpty = function () {
        return !this.time_queue.length;
    };
    return QueueModel;
}());
//# sourceMappingURL=Model.js.map