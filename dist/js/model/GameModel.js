"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var GameModel = (function () {
    function GameModel() {
    }
    GameModel.prototype.declare = function (key, defaultValue) {
        this[key] = defaultValue;
    };
    GameModel.prototype.get = function (key) {
        if (!this.hasOwnProperty(key))
            return console.log("Property doesn't exist in model.");
        return this[key];
    };
    GameModel.prototype.set = function (key, newValue) {
        if (!this.hasOwnProperty(key))
            return console.log("Property doesn't exist in model.");
        this[key] = newValue;
    };
    return GameModel;
}());
exports.default = GameModel;
//# sourceMappingURL=GameModel.js.map